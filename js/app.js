
//I initally attempted to load these from a file but couldn't due to a x-site-request-header issue...
//I'd need a webserver to load them from a file for real
var providers = [
    {"last_name": "Harris", "first_name": "Mike", "email_address": "mharris@updox.com", "specialty": "Pediatrics", "practice_name": "Harris Pediatrics"},
    {"last_name": "Wijoyo", "first_name": "Bimo", "email_address": "bwijoyo@updox.com", "specialty": "Podiatry", "practice_name": "Wijoyo Podiatry"},
    {"last_name": "Rose", "first_name": "Nate", "email_address": "nrose@updox.com", "specialty": "Surgery", "practice_name": "Rose Cutters"},
    {"last_name": "Carlson", "first_name": "Mike", "email_address": "mcarlson@updox.com", "specialty": "Orthopedics", "practice_name": "Carlson Orthopedics"},
    {"last_name": "Witting", "first_name": "Mike", "email_address": "mwitting@updox.com", "specialty": "Pediatrics", "practice_name": "Witting’s Well Kids Pediatrics"},
    {"last_name": "Juday", "first_name": "Tobin", "email_address": "tjuday@updox.com", "specialty": "General Medicine", "practice_name": "Juday Family Practice"}
];

var providersDirectory = function(providers){
    var self = this;
    //The master list of providers
    self.providers = ko.observableArray(providers)

    //These are only used for adding a new provider
    self.lastName = ko.observable();
    self.firstName = ko.observable();
    self.email = ko.observable();
    self.specialty = ko.observable();
    self.practiceName = ko.observable();

    //Adding a new provider
    self.addNewProvider = function(){
        //Validate the form before adding the new values
        var providersForm = $('form');
        providersForm.parsley().validate();
        if(!providersForm.parsley().isValid()){
            sweetAlert("Oops...", "Your input has errors, please correct all errors and try again!", "error");
            return;
        }

        self.providers.unshift({
            "last_name":self.lastName(),
            "first_name":self.firstName(),
            "email_address":self.email(),
            "specialty":self.specialty(),
            "practice_name":self.practiceName()
        })

        sweetAlert("Success!", "Successfully added new provider " + self.firstName() + ' ' + self.lastName() + '!', "success");

        //Clear out the provider we just added from the field list
        self.lastName(undefined);
        self.firstName(undefined);
        self.email(undefined);
        self.specialty(undefined);
        self.practiceName(undefined);

    }

    //Sorting providers
    self.sort = ko.observable();
    self.sort.subscribe(function(newValue){

        //Exit if there's no sorting filter applied
        if(newValue == '' || newValue == undefined){
            return;
        }

        self.providers.sort(function(provider1, provider2){

            var comparisonValue1 = null;
            var comparisonValue2 = null;

            //First, find the values we're going to be comparing
            if(newValue.indexOf('firstName') > -1){
                comparisonValue1 = provider1.first_name;
                comparisonValue2 = provider2.first_name;
            }
            if(newValue.indexOf('lastName') > -1){
                comparisonValue1 = provider1.last_name;
                comparisonValue2 = provider2.last_name;
            }
            if(newValue.indexOf('emailAddress') > -1){
                comparisonValue1 = provider1.email_address;
                comparisonValue2 = provider2.email_address;
            }
            if(newValue.indexOf('specialty') > -1){
                comparisonValue1 = provider1.specialty;
                comparisonValue2 = provider2.specialty;
            }
            if(newValue.indexOf('practiceName') > -1){
                comparisonValue1 = provider1.practice_name;
                comparisonValue2 = provider2.practice_name;
            }

            //Next find the order we're comparing them in
            if(newValue.indexOf('Asc') > -1){
                return comparisonValue1 > comparisonValue2;
            }

            //If it's not ascending, it has to be descending
            return comparisonValue1 < comparisonValue2;

        });

    })


    //Removing providers
    self.remove = function(){
        $('.deleteCheck:checked').each(function(){
            self.providers.splice($(this).attr('dataId'), 1);
        })
    }

}

var viewModel = new providersDirectory(providers);

ko.applyBindings(viewModel);


$(document).ready(function () {
    $('form').parsley().subscribe('parsley:form:validate', function (formInstance) {

        // if one of these blocks is not failing do not prevent submission
        // we use here group validation with option force (validate even non required fields)
        if (formInstance.isValid('block1', true) || formInstance.isValid('block2', true)) {
            $('.invalid-form-error-message').html('');
            return;
        }
        // else stop form submission
        formInstance.submitEvent.preventDefault();

        // and display a gentle message
        $('.invalid-form-error-message')
            .html("You must correctly fill the fields of at least one of these two blocks!")
            .addClass("filled");
        return;
    });

});
